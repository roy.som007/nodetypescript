import express from 'express';
import config from './config/config';
import routeV1 from './api/v1/routes';
import routeV2 from './api/v2/routes';


const app = express();

/** Parse the body of the request */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api/v1", routeV1);
app.use("/api/v2", routeV2);

app.listen(config.server.port, () => console.log(`Server is running ${config.server.hostname}:${config.server.port}`));