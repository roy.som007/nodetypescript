import express from 'express';
import { Request, Response } from 'express';
import respHandler from '../../library/response';
import parserController from '../controller/parserController';

const router = express.Router();

router.post('/parse', (req: Request, res: Response) => {
    respHandler.finalResponse(res, parserController.parserData(req));
});

export = router;