import { Request } from 'express';

const parserData = (reqData: Request) => {
    try {
        
        if(reqData.body.data){

            let rawData = reqData.body.data;
            // Prepare final data
            return {
                data: {
                    firstName: rawData.substring(0, 8).replace(/0/g, ""),
                    lastName: rawData.substring(8, 18).replace(/0/g, ""),
                    clientID: rawData.substring(18, 25).replace(/(\d{3})(\d{4})/, "$1-$2")
                }
            }

        } else {
            // when no data available
            return {
                data: "Not a valid data!"
            }
        }
    } catch (error) {
        // return error statement
        return {
            code: 401,
            data: error.message
        };
    }
};

export default { parserData };
