import { Response } from 'express';

const respHandler = {

	finalResponse: (res: Response, result: any) => {
        try {
            let resp = {
                statusCode: result.code || 200,
                data: result.data
            }
            return res.json(resp);
        } catch (error) {

            let code = error.code || 500;

            let resp = {
                statusCode: code,
                message: error
            }
            return res.json(resp);
        }
	}
}

export default respHandler;