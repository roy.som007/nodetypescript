# NodeTypescript

**Install Node Packages**

`npm i` in the root folder

**Run in Dev Move**

`npm run dev`

_It should run on localhost:3000_

**Build the Project**

`npm run build`

**Run on Prod**

`npm run start`
